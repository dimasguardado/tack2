# TackDB

TackDB is a small, single-user, non-durable, transactional key-value database
written in Clojure. It was written as a coding exercise and is not intended
by any means for production usage. The design of the API is part of
[Thumbtack, Inc.'s Software Engineer Challenges](http://www.thumbtack.com/challenges/software-engineer).

## Installation

The project can be cloned from git@gitlab.com:dimasguardado/tack2.git
and built with Leiningen

```bash
$ git clone git@gitlab.com:dimasguardado/tack2.git
$ cd tack2
$ lein uberjar
```

## Usage

Once a jar is built, it can be invoked in the command-line, like so:

    $ java -jar target/uberjar/tack-db-2.0.0-SNAPSHOT-standalone.jar

## License

Copyright © 2014 Dimas Guardado, Jr. All rights reserved.
