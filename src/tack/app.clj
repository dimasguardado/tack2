(ns tack.app
  (:require [tack.core :as tack]
            [tack.repl :as repl]
            [clojure.java.io :as io])
  (:gen-class))

(defn start-app []
  (doall
   (repl/new-repl
    (tack/new-db)
    (line-seq (io/reader *in*)))))

(defn -main
  "Initializes the Tack DB interactive REPL"
  [& args]
  (println "Welcome to Tack DB! Please enter a command.")
  (start-app)
  (println "Bye for now!"))
