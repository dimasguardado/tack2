(ns tack.repl
  (:refer-clojure :exclude [read])
  (:require [clojure.string :as s]
            [tack.util :as util]))

(defn read [statement]
  (let [[head & args] (s/split (s/triml statement) #"\s+")
        head          (s/upper-case head)]
    (case head
      "END" :end
      (cons (symbol head) args))))

(defn new-evaluator [db]
  (fn [statement]
    (let [[head & args]  statement
          operation      (ns-resolve 'tack.core head)
          expected-args  (dec (util/argslength operation))
          actual-args    (count args)]
      (cond
       (not  (util/user-op? operation)) :unsupported
       (not= expected-args actual-args) ^{ :expected expected-args
                                           :actual   actual-args }  #{:arity}
       :else (apply operation (cons db args))))))

(defn format-val [val]
  (case val
    nil "NULL"
    :void ""
    :no-tx "NO TRANSACTION"
    :unsupported "COMMAND NOT SUPPORTED"
    #{:arity}
    (let [{:keys [expected actual]} (meta val)]
      (format
       "WRONG ARGUMENTS - EXPECTED: %1$s, ACTUAL: %2$s"
       expected actual))
    (str val)))

(defn print-val [val]
  (let [result (format-val val)]
    (if (not (empty? result))
      (println result))
    result))

(defn new-repl [db lines]
  (let [eval (new-evaluator db)]
    (->> lines
         (map s/trim)
         (remove empty?)
         (map read)
         (take-while (partial not= :end))
         (map eval)
         (map print-val))))
