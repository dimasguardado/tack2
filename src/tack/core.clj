(ns tack.core
  (:require [tack.util :as util]))

(defn new-db

  ([] (new-db {}))

  ([values]
     (let [counts (frequencies (vals values))]
       { :values (ref values)
         :counts (ref counts)
         :stack  (ref '()) })))

(defn ^:user-op SET [db name value]
  (let [{:keys [values counts]} db]
    (dosync
     (let [old (@values name)]
       (alter values assoc name value)
       (alter counts util/replace-count old value)
       :void))))

(defn ^:user-op UNSET [db name]
  (let [{:keys [values counts]} db]
    (dosync
     (let [value (@values name)]
       (alter values dissoc name)
       (alter counts util/dec-or-remove value)
       :void))))

(defn ^:user-op GET [db name]
  (let [{:keys [values]} db]
    (@values name)))

(defn ^:user-op NUMEQUALTO [db value]
  (let [{:keys [counts]} db]
    (or (@counts value) 0)))

(defn ^:user-op BEGIN [db]
  (let [{:keys [values counts stack]} db]
    (dosync
     (alter stack conj { :values @values :counts @counts })
     :void)))

(defn ^:user-op COMMIT [db]
  (let [{:keys [stack]} db]
    (dosync
     (if (empty? @stack) :no-tx
         (do
           (ref-set stack '())
           :void)))))

(defn ^:user-op ROLLBACK [db]
  (let [{:keys [values counts stack]} db]
    (dosync
     (if (empty? @stack) :no-tx
         (let [{vs :values cs :counts} (peek @stack)]
           (ref-set values vs)
           (ref-set counts cs)
           (alter stack pop)
           :void)))))
