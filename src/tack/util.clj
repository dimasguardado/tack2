(ns tack.util)

(def inc-count (fnil inc 0))

(defn dec-or-remove [counts value]
  (let [count (or (counts value) 0)]
    (if (<= count 1)
      (dissoc counts value)
      (update-in counts [value] dec))))

(defn inc-or-add [counts value]
  (update-in counts [value] inc-count))

(defn replace-count [counts old new]
  (-> counts
      (dec-or-remove old)
      (inc-or-add new)))

(defn argslength [fn]
  (-> fn
      meta
      :arglists
      first
      count))

(defn user-op? [fn]
  (-> fn
      meta
      :user-op))
