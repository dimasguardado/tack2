(defproject tack-db "2.0.0-SNAPSHOT"
  :description "A small, single-user, non-durable, transactional key-value database in Clojure"
  :url "https://gitlab.com/dimasguardado/tack2"
  :dependencies [[org.clojure/clojure "1.6.0"]]
  :main ^:skip-aot tack.app
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
