(ns tack.app-test
  (:require [clojure.test :refer :all]
            [tack.app :refer [start-app]]
            [clojure.java.io :as io]))

(defn run-script [script-file]
  (with-open [script (io/reader script-file)]
    (with-out-str
      (binding [*in* script]
        (start-app)))))

(with-test
  (defn test-script [script-name]
    (let [script-file (str "test/tack/" script-name "-in.txt")
          expect-file (str "test/tack/" script-name "-out.txt")
          expected-out (slurp expect-file)]
      (is (= (run-script script-file) expected-out)))))

(deftest integration-test

  (testing "data commands"
    (test-script "basic-data")
    (test-script "count-data"))

  (testing "transaction commands"
    (test-script "rollback-tx")
    (test-script "commit-tx")
    (test-script "mixed-tx")
    (test-script "count-tx")))
