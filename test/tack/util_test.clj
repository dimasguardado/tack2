(ns tack.util-test
  (:require [clojure.test :refer :all]
            [tack.util :refer :all]))

(deftest test-dec-or-remove
  (testing "Removes from map if decrementing value would result in 0"
    (is (= (dec-or-remove { :a 3 } :a) { :a 2 }))
    (is (= (dec-or-remove { :a 2 } :a) { :a 1 }))
    (is (= (dec-or-remove { :b 1 } :b) {}))
    (is (= (dec-or-remove { :c 0 } :c) {}))
    (is (= (dec-or-remove { :d -1 } :d) {}))
    (is (= (dec-or-remove { :e 5 } :f ) { :e 5 }))))

(deftest test-inc-or-add
  (testing "Increments mapped value, or maps value to 1 if absent"
    (is (= (inc-or-add { } :a) { :a 1 }))
    (is (= (inc-or-add { :b 3 } :b) { :b 4 }))))

(deftest test-replace-count
  (testing "Decrements old value, increments new value"
    (is (= (replace-count { :a 3 :b 7 } :a :b) { :a 2 :b 8 }))
    (is (= (replace-count { :c 9 :d 12 } :d :c) { :c 10 :d 11 }))
    (is (= (replace-count { :e 4 } :e :f)) { :e 3 :f 1 })
    (is (= (replace-count { :g 5 } :h :g)) { :g 6 })
    (is (= (replace-count { :i 1 :j 2 } :i :j) { :j 3 }))
    (is (= (replace-count { :k 8 :l 1 } :l :k) { :k 9 }))))

(deftest test-argslength
  (testing "returns size of function arg list"
    (is (= (argslength (ns-resolve 'tack.core 'GET)) 2))
    (is (= (argslength (ns-resolve 'tack.core 'SET)) 3))
    (is (= (argslength (ns-resolve 'tack.core 'COMMIT)) 1))))

(deftest test-user-op
  (testing "returns true if function is marked as accesible to user"
    (is (true? (user-op? (ns-resolve 'tack.core 'GET)))
        (false? (user-op? (ns-resolve 'tack.core 'new-db))))))
