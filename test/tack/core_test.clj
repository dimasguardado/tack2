(ns tack.core-test
  (:require [clojure.test :refer :all]
            [tack.core :refer :all]))

(deftest db-get
  (testing "Get values from database"
    (let [db (new-db { "ex" "10" "zee" "20"})]
      (is (= (GET db "why") nil))
      (is (= (GET db "ex") "10"))
      (is (= (GET db "zee") "20")))))

(deftest db-numequalto
  (testing "Count database values"
    (let [db (new-db { "a" "10" "b" "20" "c" "10"})]
      (is (= (NUMEQUALTO db "10") 2))
      (is (= (NUMEQUALTO db "20") 1))
      (is (= (NUMEQUALTO db "30") 0)))))

(deftest db-set

  (testing "Insert values into database"
    (let [db (new-db)]

      (SET db "ex" "10")

      (is (= (GET db "ex") "10"))
      (is (= (NUMEQUALTO db "10") 1))))

  (testing "Update existing values in database"
    (let [db (new-db { "ex" "10" })]

      (SET db "ex" "20")

      (is (= (GET db "ex") "20"))
      (is (= (NUMEQUALTO db "20") 1))
      (is (= (NUMEQUALTO db "10") 0)))))

(deftest db-unset

  (testing "Unset absent key from database"
    (let [db (new-db)]

      (UNSET db "ex")

      (is (= (GET db "ex") nil))))

  (testing "Unset present key from database"
    (let [db (new-db { "ex" "10" })]

      (UNSET db "ex")

      (is (= (GET db "ex") nil))
      (is (= (NUMEQUALTO db "10") 0))))

  (testing "Unset with multiple values"
    (let [db (new-db { "a" "10" "b" "20" "c" "10"})]

      (UNSET db "a")

      (is (= (GET db "a") nil))
      (is (= (NUMEQUALTO db "10") 1)))))

(deftest basic-command-script

  (testing "Value mutation"
    (let [db (new-db)]

      (SET db "ex" "10")

      (is (= (GET db "ex") "10"))

      (UNSET db "ex")

      (is (= (GET db "ex") nil))))

  (testing "Value count mutation"
    (let [db (new-db)]

      (SET db "a" "10")
      (SET db "b" "10")

      (is (= (NUMEQUALTO db "10") 2))
      (is (= (NUMEQUALTO db "20") 0))

      (SET db "b" "30")

      (is (= (NUMEQUALTO db "10") 1)))))

(deftest transaction-command-script

  (testing "Basic nested transaction Rollback"
    (let [db (new-db)]

      (BEGIN db)
      (SET db "a" "10")

      (is (= (GET db "a") "10"))

      (BEGIN db)
      (SET db "a" "20")

      (is (= (GET db "a") "20"))

      (ROLLBACK db)

      (is (= (GET db "a") "10"))

      (ROLLBACK db)

      (is (= (GET db "a") nil))))

  (testing "Basic transaction commit"
    (let [db (new-db)]

      (BEGIN db)
      (SET db "a" "30")

      (BEGIN db)
      (SET db "a" "40")

      (COMMIT db)

      (is (= (GET db "a") "40"))

      (is (= (ROLLBACK db) :no-tx))))

  (testing "Mixed rollback and commit"
    (let [db (new-db)]

      (SET db "a" "50")

      (BEGIN db)

      (is (= (GET db "a") "50"))

      (SET db "a" "60")

      (BEGIN db)
      (UNSET db "a")

      (is (= (GET db "a") nil))

      (ROLLBACK db)
      (is (= (GET db "a") "60"))

      (COMMIT db)
      (is (= (GET db "a") "60"))))

  (testing "Transactional counts"
    (let [db (new-db)]

      (SET db "a" "10")

      (BEGIN db)
      (is (= (NUMEQUALTO db "10") 1))

      (BEGIN db)
      (UNSET db "a")
      (is (= (NUMEQUALTO db "10") 0))

      (ROLLBACK db)
      (is (= (NUMEQUALTO db "10") 1))

      (COMMIT db))))
