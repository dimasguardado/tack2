(ns tack.repl-test
  (:refer-clojure :exclude [read])
  (:require [clojure.test :refer :all]
            [tack.repl :refer :all]
            [tack.core :refer :all])
  (:import [clojure.lang ArityException]))

(deftest reading

  (testing "read end"
    (is (= (read "END") :end))
    (is (= (read " END ") :end))
    (is (= (read " end") :end)))

  (testing "read operations"
    (is (= (read "SET ex 10") '(SET "ex" "10")))
    (is (= (read " UNSET ex") '(UNSET "ex")))
    (is (= (read "COMMIT ") '(COMMIT)))
    (is (= (read "   \tinvalid extra    args now  \t")
           '(INVALID "extra" "args" "now")))))

(deftest evaluating

  (testing "result evaluation"
    (let [db (new-db { "ex" "10" "why" "20" "zee" "10" })
          eval (new-evaluator db)]

      (is (= (eval '(GET "ex")) "10"))
      (is (= (eval '(GET "blerg")) nil))
      (is (= (eval '(GET "why")) "20"))

      (let [result (eval '(GET "why" "are" "wee"))]
        (is (= result #{:arity}))
        (is (= (meta result) { :expected 1 :actual 3 })))

      (let [result (eval '(GET))]
        (is (= result #{:arity}))
        (is (= (meta result) { :expected 1 :actual 0 })))


      (is (= (eval '(NUMEQUALTO "10")) 2))
      (is (= (eval '(NUMEQUALTO "20")) 1))
      (is (= (eval '(NUMEQUALTO "30")) 0))

      (let [result (eval '(NUMEQUALTO "20" "blerg"))]
        (is (= result #{:arity}))
        (is (= (meta result) { :expected 1 :actual 2 })))

      (let [result (eval '(NUMEQUALTO))]
        (is (= result #{:arity}))
        (is (= (meta result)) { :expected 1 :actual 0 }))))

  (testing "mutation evaluation"
    (let [db (new-db { "a" "10" "b" "20" "c" "10" })
          eval (new-evaluator db)]

      (is (= (eval '(SET "b" "30")) :void))
      (is (= (eval '(GET "b")) "30"))

      (is (= (eval '(UNSET "c")) :void))
      (is (= (eval '(GET "c")) nil))))

  (testing "transaction evaluation"
    (let [db (new-db { "a" "10" "b" "20" "c" "10" })
          eval (new-evaluator db)]

      (is (= (eval '(ROLLBACK)) :no-tx))
      (is (= (eval '(COMMIT)) :no-tx))

      (is (= (eval '(BEGIN)) :void))
      (eval '(SET "d" "30"))
      (eval '(BEGIN))
      (eval '(SET "d" "40"))
      (is (= (eval '(ROLLBACK)) :void))
      (is (= (eval '(GET "d")) "30"))
      (is (= (eval '(COMMIT))))
      (is (= (eval '(GET "d")) "30")))))

(deftest formatting
  (testing "result value formatting"
    (is (= (format-val nil) "NULL"))
    (is (= (format-val :void) ""))
    (is (= (format-val :no-tx) "NO TRANSACTION"))
    (is (= (format-val "stuff") "stuff"))
    (is (= (format-val 42) "42"))))

(deftest integration
  (testing "test the integrated repl"
    (let [db (new-db)
          script ["SET a 50"
                  "BEGIN"
                  "GET a"
                  "SET a 60"
                  "BEGIN"
                  "UNSET a"
                  "GET a"
                  "ROLLBACK"
                  "GET a"
                  "NUMEQUALTO 60"
                  "COMMIT"
                  "GET a"
                  "ROLLBACK"
                  "COMMIT"
                  "END"
                  "GET a"
                  "GET b"]]

      (is (= (doall (new-repl db script))
             ["" "" "50" "" "" "" "NULL" ""
              "60" "1" "" "60" "NO TRANSACTION" "NO TRANSACTION"])))))
